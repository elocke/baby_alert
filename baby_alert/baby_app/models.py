from django.db import models
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from multiselectfield import MultiSelectField
from phonenumber_field.modelfields import PhoneNumberField


# Create your models here.
@python_2_unicode_compatible
class Person(models.Model):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_('Name'), blank=True, max_length=255)
    email = models.EmailField(_('Email'), blank=True, null=True, unique=True, default=None)
    phone = PhoneNumberField(_('Phone Number'), null=True, blank=True, unique=True)

    date_signup = models.DateTimeField(auto_now_add=True)

    # protect the db from saving any blank fields (from admin or your app form)
    def save(self, *args, **kwargs):
        if self.email is not None and self.email.strip() == "":
            self.email = None

        models.Model.save(self, *args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'People'


@python_2_unicode_compatible
class Message(models.Model):
    """
    Message to be sent. Can include photo. 
    """
    body = models.TextField(_('Message body'))
    photo = models.ImageField(_('Photo Attachment'), upload_to='.', null=True, blank=True)
    recipient = models.ManyToManyField(Person,
                                       help_text=_('People that received this message'),
                                       through='Recipient',
                                       through_fields=('sent_message', 'receiving_person'),
                                       )
    date_sent = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {}".format(self.body[:10], self.date_sent)


@python_2_unicode_compatible
class SMSMessageStatus(models.Model):
    """
    Track Twilio message callbacks for status checking
    """
    message_sid = models.CharField(max_length=40, unique=True)
    status = models.CharField(max_length=30, null=True, blank=True)
    error_code = models.IntegerField(null=True, blank=True)
    date_sent = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.status, self.error_code)


SEND_METHODS = (('email', 'Email'),
                ('sms', 'SMS'))


@python_2_unicode_compatible
class Recipient(models.Model):
    """
    Through table for many-to-many relationship between person & message. Allows tracking of the individual messages 
    sent.
    """
    sent_message = models.ForeignKey(Message, on_delete=models.CASCADE, related_name='received_message')
    receiving_person = models.ForeignKey(Person, on_delete=models.CASCADE)
    time_sent = models.DateTimeField(auto_now_add=True)

    send_method = MultiSelectField(choices=SEND_METHODS)

    message_status = models.OneToOneField(SMSMessageStatus, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        return "{:10} - {}".format(self.sent_message.body, self.receiving_person.name)

    class Meta:
        unique_together = (('sent_message', 'receiving_person'),)
