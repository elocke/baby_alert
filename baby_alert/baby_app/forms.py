from django import forms
from django.core.mail import send_mail, EmailMultiAlternatives
from django.utils.translation import ugettext_lazy as _

from anymail.message import attach_inline_image_file
from django.conf import settings
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Submit, Div, Field, HTML
from crispy_forms.bootstrap import FormActions

from .models import Message, Person, Recipient, SMSMessageStatus

from twilio.base.exceptions import TwilioRestException

class PersonSignupForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['name', 'email', 'phone']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_id = 'id-signupform'
        self.helper.form_class = 'babyForms'
        self.helper.form_method = 'post'
        # self.helper.form_action = 'web-signup'
        self.helper.help_text_inline = True
        self.helper.form_show_labels = False

        # self.helper.add_input(Submit('submit', 'Submit'))

        self.helper.layout = Layout(
            HTML("""
                <h1 class="text-center">Baby Greyson Alerts!</br>
                <small class="text-muted text-center">Sign up now or text your first and last name to (720) 709-2496.</small></h1>"""),
            Field('name', placeholder='Name'),
            Field('email', placeholder='Email Address'),
            Field('phone', placeholder='Phone Number'),

            FormActions(
                Submit('submit', 'Submit', css_class='button'),
                css_class=' text-center',
            )
        )
        super(PersonSignupForm, self).__init__(*args, **kwargs)

    def send_email(self):
        # send email using the self.cleaned_data dictionary
        send_mail("Web sign-up.",
                  """{name} has signed up online.\r\n\r\nEmail: {email}\r\n\r\nPhone: {phone}""".format(**self.cleaned_data),
                  "babyalert@babylocke.life",
                  ["babyalert@evanlocke.me"])

    def send_sms(self):
        if self.cleaned_data.get('phone', None):
            try:
                settings.TWILIO_CLIENT.messages.create(
                    to=str(self.cleaned_data['phone']),
                    from_=settings.TWILIO_DEFAULT_CALLERID,
                    body=_('Thank you for signing up for baby alerts {name}').format(**self.cleaned_data)
                )
            except TwilioRestException:
                print('Error with twilio')


class SendMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['body', 'photo']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_id = 'id-sendmessageform'
        self.helper.form_class = 'babyForms'
        self.helper.form_method = 'post'
        # self.helper.form_action = 'web-signup'
        self.helper.help_text_inline = True
        self.helper.form_show_labels = False

        self.helper.layout = Layout(
            HTML("""
                <h1 class="text-center">Send Alert!</h1>"""),
            Field('body', placeholder='Message body'),
            Field('photo', placeholder='Photo'),

            FormActions(
                Submit('submit', 'Submit', css_class='button'),
                css_class=' text-center',
            )
        )
        super(SendMessageForm, self).__init__(*args, **kwargs)

    def send_message(self):
        # send email using the self.cleaned_data dictionary
        emails_to_send = list(Person.objects.filter(email__isnull=False).values_list('email', flat=True))
        msg = EmailMultiAlternatives(
            subject="Baby Greyson Alert!",
            body=self.cleaned_data['body'],
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=["me@evanlocke.me"],
            bcc=emails_to_send,
            reply_to=["Evan <me@evanlocke.me>", ])

        photo = self.cleaned_data.get('photo', None)

        if photo:
            baby_photo = attach_inline_image_file(msg, 'baby_alert' + self.instance.photo.url)
            html = """<img alt="Baby pic" src="cid:{baby_photo}">""".format(baby_photo=baby_photo)
            msg.attach_alternative(html, "text/html")

        msg.send()

        sms_to_send = Person.objects.filter(phone__isnull=False)
        # send twilio sms
        for person in sms_to_send:
            recipient, created = Recipient.objects.update_or_create(sent_message=self.instance,
                                                                    receiving_person=person,
                                                                    send_method='sms')
            message_status = SMSMessageStatus.objects.create(recipient=recipient)

            sms_message = settings.TWILIO_CLIENT.messages.create(
                to=str(person.phone),
                from_=settings.TWILIO_DEFAULT_CALLERID,
                body=_('{body}').format(**self.cleaned_data),
                status_callback='https://babylocke.life/twilio_status_callback/{}/'.format(message_status.id),
                media_url='https://babylocke.life/' + self.instance.photo.url
            )
            message_sid = getattr(sms_message, 'sid', None)
            if message_sid:
                message_status.message_sid = message_sid
                message_status.save()

