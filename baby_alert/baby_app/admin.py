from django.contrib import admin

# Register your models here.
from .models import Message, SMSMessageStatus, Person, Recipient

admin.site.register(Recipient)


class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'phone')

admin.site.register(Person, PersonAdmin)


class MessageAdmin(admin.ModelAdmin):
    list_display = ('date_sent', 'body')

admin.site.register(Message, MessageAdmin)


class SMSMessageStatusAdmin(admin.ModelAdmin):
    list_display = ( 'status', 'error_code')

admin.site.register(SMSMessageStatus, SMSMessageStatusAdmin)
