from django.apps import AppConfig


class BabyAppConfig(AppConfig):
    name = 'baby_alert.baby_app'
