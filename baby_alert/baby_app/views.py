from django.shortcuts import render

# Create your views here.
from twilio import twiml
from twilio.twiml.messaging_response import MessagingResponse
from django_twilio.decorators import twilio_view
from django_twilio.request import decompose

from django.http import Http404, HttpResponse
from django.views.generic import FormView, TemplateView
from django.views.generic.edit import ModelFormMixin
from django.contrib.auth.mixins import PermissionRequiredMixin

from .models import Person, SMSMessageStatus
from .forms import PersonSignupForm, SendMessageForm


@twilio_view
def twilio_inbound(request):
    """
    View for signing up for alerts via text message. Creates a Person and sets the sender's phone number as the Person's
    number.

    :param request:
    :return: Confirmation message
    """
    response = MessagingResponse()

    # Create a new TwilioRequest object
    twilio_request = decompose(request)

    # If there are not distinctly 2 words, do not add person to the db. Assume is a bad message. Accept first + last.
    if len(str(twilio_request.body).split(' ')) != 2:
        response.message('Please submit a first and last name separated by a space.')
        return response

    created = False
    try:
        person, created = Person.objects.update_or_create(name=twilio_request.body, phone=twilio_request.from_)
    except:
        pass

    # Discover the type of request
    if twilio_request.type is 'message':
        if created:
            response.message('Thanks for signing up for baby alerts. You will receive a text when something happens.')
        else:
            response.message('Thanks for updating your name on baby alerts. You will receive a text when something happens.')
        return response

    return response


class WebSignup(FormView):
    template_name = 'baby_app/web_signup.html'
    form_class = PersonSignupForm
    success_url = '/thanks/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.

        form.object = form.save()
        form.send_email()
        form.send_sms()
        return super(WebSignup, self).form_valid(form)


class SendMessage(PermissionRequiredMixin, FormView):
    permission_required = 'is_staff'
    template_name = 'baby_app/send_message.html'
    form_class = SendMessageForm
    success_url = '/message_sent/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.

        form.object = form.save()
        form.send_message()
        return super(SendMessage, self).form_valid(form)


class Thanks(TemplateView):
    template_name = 'baby_app/thanks.html'


class MessageSent(TemplateView):
    template_name = 'baby_app/message_sent.html'


@twilio_view
def twilio_status_callback(request, id):
    # Create a new TwilioRequest object
    twilio_request = decompose(request)

    try:
        message_status = SMSMessageStatus.objects.get(id=id, message_sid=twilio_request.smssid)
        message_status.status = twilio_request.messagestatus
        if getattr(twilio_request, 'errorcode', None):
            message_status.error_code = twilio_request.errorcode
        message_status.save()
    except:
        raise Http404("Poll does not exist")

    return HttpResponse(content_type="application/json", status=200)
