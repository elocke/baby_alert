from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^$',
        view=views.WebSignup.as_view(),
        name='web-signup'
    ),
    url(
        regex=r'^send_message/$',
        view=views.SendMessage.as_view(),
        name='send-message'
    ),
    url(
        regex=r'thanks/$',
        view=views.Thanks.as_view(),
        name='thanks'
    ),
    url(
        regex=r'message_sent/$',
        view=views.MessageSent.as_view(),
        name='message-sent'
    ),
    url(
        regex=r'twilio_inbound/$',
        view=views.twilio_inbound,
        name='twilio-inbound'
    ),
    url(
        regex=r'twilio_status_callback/(?P<id>\d+)/$',
        view=views.twilio_status_callback,
        name='twilio-status-callback'
    ),
]
